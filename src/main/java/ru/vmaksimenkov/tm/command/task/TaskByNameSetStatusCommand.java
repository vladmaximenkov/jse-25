package ru.vmaksimenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.exception.entity.TaskNotFoundException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskByNameSetStatusCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Set task status by name";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SET TASK STATUS]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        if (!serviceLocator.getTaskService().existsByName(userId, name)) throw new TaskNotFoundException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        serviceLocator.getTaskService().setTaskStatusByName(userId, name, Status.getStatus(TerminalUtil.nextLine()));
    }

    @NotNull
    @Override
    public String name() {
        return "task-set-status-by-name";
    }

}
