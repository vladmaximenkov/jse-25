package ru.vmaksimenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[TASK CLEAR]");
        serviceLocator.getTaskService().clear(userId);
    }

    @NotNull
    @Override
    public String name() {
        return "task-clear";
    }

}
