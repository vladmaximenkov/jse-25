package ru.vmaksimenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.exception.entity.TaskNotFoundException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

public final class TaskByIdUnbindCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Unbind task by id";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UNBIND TASK BY ID]");
        if (serviceLocator.getTaskService().size(userId) < 1) throw new TaskNotFoundException();
        System.out.println("ENTER TASK ID:");
        serviceLocator.getProjectTaskService().unbindTaskFromProject(userId, TerminalUtil.nextLine());
    }

    @NotNull
    @Override
    public String name() {
        return "task-unbind-by-id";
    }

}
