package ru.vmaksimenkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.util.TerminalUtil;

public final class ProjectByIndexViewCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "View project by index";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        showProject(serviceLocator.getProjectService().findOneByIndex(userId, TerminalUtil.nextNumber()));
    }

    @NotNull
    @Override
    public String name() {
        return "project-view-by-index";
    }

}
