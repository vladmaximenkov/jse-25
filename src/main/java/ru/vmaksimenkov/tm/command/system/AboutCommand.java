package ru.vmaksimenkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String description() {
        return "Show developer info";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Vlad Maksimenkov");
        System.out.println("vmaksimenkov@tsconsulting.com");
    }

    @NotNull
    @Override
    public String name() {
        return "about";
    }

}
