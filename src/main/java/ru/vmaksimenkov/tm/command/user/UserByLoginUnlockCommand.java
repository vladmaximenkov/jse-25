package ru.vmaksimenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.enumerated.Role;

import static ru.vmaksimenkov.tm.util.TerminalUtil.nextLine;

public final class UserByLoginUnlockCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Unlock user by login";
    }

    @Override
    public void execute() {
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER LOGIN:");
        serviceLocator.getUserService().unlockUserByLogin(nextLine());
    }

    @NotNull
    @Override
    public String name() {
        return "user-unlock-by-login";
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
