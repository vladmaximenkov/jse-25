package ru.vmaksimenkov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.service.ServiceLocator;
import ru.vmaksimenkov.tm.enumerated.Role;

@NoArgsConstructor
public abstract class AbstractCommand {

    @NotNull protected ServiceLocator serviceLocator;

    @Nullable public abstract String arg();

    @Nullable public abstract String description();

    public abstract void execute();

    @NotNull public abstract String name();

    @Nullable
    public Role[] roles() {
        return null;
    }

    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}