package ru.vmaksimenkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(@NotNull AbstractCommand command);

    @NotNull
    Collection<AbstractCommand> getArguments();

    @NotNull
    Collection<String> getCommandArgs();

    @Nullable
    AbstractCommand getCommandByArg(@NotNull String name);

    @Nullable
    AbstractCommand getCommandByName(@NotNull String name);

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    Collection<AbstractCommand> getCommands();

}