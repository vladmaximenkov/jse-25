package ru.vmaksimenkov.tm.exception.entity;

public class ProjectNotFoundException extends RuntimeException {

    public ProjectNotFoundException() {
        super("Error! Project not found...");
    }

}
