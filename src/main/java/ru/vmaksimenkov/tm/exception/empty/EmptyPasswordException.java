package ru.vmaksimenkov.tm.exception.empty;

import ru.vmaksimenkov.tm.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error! Password is empty...");
    }

}
